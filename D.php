<?php
// some sort of Dbug helper :)
// version 1.2
class D
{
	const DUMP_FILE_PATH = 'D:/d.log';
	const TRACE_FORMAT_SMALL = 'small';
	const KILL_DUMP_ON_EACH_REQUEST = false;
	const AUTO_OUTPUT_LINE = true; // if true - dumps file and line from which dump was made

	private static $_instance = null;
	private $_switch = true;  // on - true, off - false

	public static function _on()
	{
		$d = self::getInstance();
		$d->turnSwitch(true);
	}
	public static function _off()
	{
		$d = self::getInstance();
		$d->turnSwitch(false);
	}

	
	private static function getInstance()
	{
		if (empty(self::$_instance))
		{
			if (self::KILL_DUMP_ON_EACH_REQUEST) // erasing dump file contents
			{
				self::killDumpFile();
			}
			self::$_instance = new D();
		}
		
		return self::$_instance;
	}

	final private function __construct()
	{
		$this->_switch = true;
	}
	final private function __clone()
	{
		throw new Exception('class D() clonin\' is deprecated by ME');
	}	
	
	protected function turnSwitch($position = true)
	{
		$this->_switch = $position;
	}
	protected function status()
	{
		return $this->_switch;
	}
	

	/**
	* @param $text : string to be dumped
	* @param $descr: descrition
	* @param $asVar: if true - var_export, false - simple echo
	*
	* @return void
	*/
	public static function _d($text = null,$descr = '', $asVar = true)
	{
		$d = self::getInstance();
		if (!$d->status())
		{
			return false;
		}
		if (!$f = fopen(self::DUMP_FILE_PATH, 'a+'))
		{
			echo "Cannot open file";
			exit;
		}
		$str = $asVar ?  var_export($text, true) : $text;
		$args = func_get_args();
		if (empty($args) && self::AUTO_OUTPUT_LINE)
		{
			$str = self::_l();
		}
		$inputString = "\r\n -------------------------------- \r\n" . $str . "   **  " . $descr;
		if (fwrite($f, $inputString) === FALSE) 
		{
			echo "Cannot write to file";
			exit;
		}
		fclose($f);
	}
	
	/*
	 * erase all dump file content
	 * called from getinstance() method with condition of IF self::KILL_DUMP_ON_EACH_REQUEST 
	 */
	public static function killDumpFile()
	{
		if (!$f = fopen(ROOT_DIR . self::DUMP_FILE_PATH, 'w'))
		{
			echo "Cannot open file";
			exit;
		}
		fclose($f);
	}
	
	/**
	 *
	 */
	public static function _dRequest()
	{
		try
		{
			$fc = Zend_Controller_Front::getInstance();
			$request = $fc->getRequest();
		}
		catch (Exception $e)
		{
			self::_d($e->getMessage(), 'error getting request:');
		}
		return $request;
	}

	/*
	*	gets line, from which dump was made
	*/
	public static function _l()
	{
		$trace = self::_getTrace();
		if (!is_array($trace))
		{
			return (string) $trace;
		}
		
		foreach($trace as $key => $value)
		{
			if (array_key_exists('class', $value) && ($value['class']== 'D'))
			{
				$res = 'FILE ' . $value['file'] . '  LINE ' . $value['line'];
				continue;
			}
			break;
		}
		return $res;
	}

	/*
	* get current Trace status
	* formatted variously
	*/
	public static function _getTrace($format = null)
	{
		$trace = self::_getBaseTrace();
		switch ($format)
		{
		case self::TRACE_FORMAT_SMALL:
			$res = 'Small stack trace: 
';
			foreach ($trace as $key => $value)
			{
				$res .= $value['file'] . ' | line: ' . $value['line'] .'
';
			}
			return $res;
		default:
			return $trace;
		}
	}
	
	/*
	*	gets base trace from exception
	* @returns the Exception stack trace as an array. 
	*/
	public static function _getBaseTrace()
	{
		$trace = null;
		try
		{
			throw new Exception('Lorem');
		}
		catch (Exception $e)
		{
			$trace = $e->getTrace();
		}
		return $trace;
	}
}	
